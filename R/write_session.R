#' Write session record into history
#'
#' @param session A \code{Session} object
#' @param ask Ask confirmation before writing record
#' @param path Full path to sessions history file
#' @param extension Extension of history file
#' @param ... Other argument passed on to methods
#' @return The input \code{Session} object
#'
#' @export
setGeneric('write_session', function(session, ...){
	standardGeneric('write_session')
})

#' @rdname write_session
#' @export
setMethod('write_session', signature('Session'), function(session, path, ask = TRUE, extension = 'csv', ...){
	if (nrow(session@record) == 0) stop(
		'Session record is empty, have you built a session with select_moves() ?'
	)
	updated_session <- bind_rows(session@history, session@record)
	if (!ask){
		readr_writer(extension = extension)(path, ...)
		cat(colourise("Write session to history\n", 'green'))
	} else {
		write_answer <- dispatch_prompt(
			message = colourise('\nDo you want to add this session to the record ?', 'white')
		)
		if (toupper(write_answer) %in% c('Y', 'YES', 'YEAH')){
			readr_writer(extension = extension)(updated_session, path, ...)
			cat(colourise("Write session to history\n", 'green'))
		} else {
			cat(colourise('Cancel writing session to history\n', 'yellow'))
		}
	}
	invisible(session)
})
