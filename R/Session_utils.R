#' Enter a date manually from terminal
#'
#' Loop while text is not coercible to date.
#'
#' @return A date object
#' @keywords internal
#' @seealso \code{\link{choose_date}}, \code{\link{SessionBuilder}}
manual_date_entry <- function(){
	authorized_answer <- NA
	while(is.na(authorized_answer)){
		manual_date <- dispatch_prompt(message = 'Enter date (yyyy-mm-dd): ')
		authorized_format <- tryCatch({
			entered_date <- as.Date(manual_date)
			TRUE
		}, error = function(e) FALSE)
		if (authorized_format){
			authorized_answer <- TRUE
		} else {
			cat('Provided date was not coercible to date, please enter again.')
		}
	}
	as.Date(manual_date)
}

#' Ask user to choose a date
#'
#' Options are : 1 = today, 2 = tomorrow, 3 = manual entry
#'
#' @param today \code{boolean} Use today's date
#' @return A date object
#' @keywords internal
#' @seealso \code{\link{manual_date_entry}}, \code{\link{SessionBuilder}}
choose_date <- function(today){
	authorized_answer <- NA
	if (today){
		authorized_answer <- TRUE
		date_choice <- '1'
	}
	while (is.na(authorized_answer)){
		date_choice <- dispatch_prompt(message = paste(
			'Which day will you train ?',
			'\t1 : today',
			'\t2 : tomorrow',
			'\t3 : specific day',
			'Enter choice : ',
			sep = '\n'
		))
		if (date_choice %in% 1:3){
			authorized_answer <- TRUE
		} else {
			cat('Answer must be 1, 2 or 3.', '\n')
		}
	}
	switch(date_choice,
		'1' = Sys.Date(),
		'2' = Sys.Date() + 1,
		'3' = manual_date_entry()
	)
}

#' Get skills from session moves
#'
#' Skills are a result of joining moves with movement skills table.
#'
#' \code{str_moves_and_skills} returns a character string to nicely print
#'     results in console.
#'
#' @param session A \code{Session} object
#' @return A \code{data.frame} containing skills
#'
#' @export
setGeneric('skills_of_moves', function(session){
	standardGeneric('skills_of_moves')
})

#' @rdname skills_of_moves
#' @export
setMethod('skills_of_moves', signature('Session'), function(session){
	selected_moves <- data.frame(move_uid = session@movements, stringsAsFactors = F)
	inner_join(
		session@discipline@movement_skills,
		selected_moves,
		by = 'move_uid'
	)
})
