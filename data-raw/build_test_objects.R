library(coach)
library(magrittr)
library(readr)

PCKG_FOLDER <- normalizePath('.')
DATA_CLEAN_FOLDER <- file.path(PCKG_FOLDER, 'inst', 'extdata', 'clean')
extension = 'tsv'

ExampleDiscipline <- build_discipline(
	use_folder = T,
	source_folder = DATA_CLEAN_FOLDER,
	extension = extension,
	col_types = cols(
		.default = col_guess(),
		autorank = col_integer(),
		score = col_integer())
)
usethis::use_data(ExampleDiscipline, overwrite = T)

ExampleWorkout <- SessionBuilder(
	discipline = ExampleDiscipline,
	today = TRUE,
	sessions_path = file.path(DATA_CLEAN_FOLDER, 'sessions.tsv'),
	extension = 'tsv', col_types = cols(.default = col_guess())
) %>%
	select_domains(manual = F, n = 2) %>%
	select_moves(n = 1) %T>%
	print()
usethis::use_data(ExampleWorkout, overwrite = T)
